from django.contrib.auth.mixins import LoginRequiredMixin

# Create your views here.
from django.http import JsonResponse
from django.urls import reverse
from django.views.generic import CreateView, DetailView, FormView, ListView, TemplateView

from app.simulator.forms import ClassesForm, ScriptForm
from app.simulator.models import Classes


class IndexView(LoginRequiredMixin, ListView):
    template_name = "index.html"
    model = Classes


class ClassesCreateView(CreateView):
    template_name = 'classes_form.html'
    model = Classes
    form_class = ClassesForm


class ClassesDetailView(DetailView, FormView):
    form_class = ScriptForm
    template_name = "classes_detail.html"
    model = Classes

