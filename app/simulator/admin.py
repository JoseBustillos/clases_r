from django.contrib import admin

# Register your models here.
from app.simulator.models import Classes


class ClassesAdmin(admin.ModelAdmin):
    pass
admin.site.register(Classes, ClassesAdmin)