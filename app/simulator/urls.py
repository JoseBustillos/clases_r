from django.contrib.auth import views as auth_views
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from app.simulator.views import ClassesCreateView, ClassesDetailView, IndexView

urlpatterns = [
                  path('login', auth_views.LoginView.as_view(), {'template_name': 'registration/login.html'},
                       name='login'),
                  path('logout', auth_views.LogoutView.as_view(), {'template_name': 'index.html'}, name='logout'),
                  path('', IndexView.as_view(), name='index'),
                  path('nuevas_clases', ClassesCreateView.as_view(), name='createclasses'),
                  path('<int:pk>', ClassesDetailView.as_view(), name='classes-detail'),
              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
