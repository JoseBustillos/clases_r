from pyexpat import model

from django import forms

from app.simulator.models import Classes


class ClassesForm(forms.ModelForm):
    class Meta:
        model = Classes
        fields = '__all__'


class ScriptForm(forms.Form):
    script = forms.CharField(widget=forms.Textarea)
