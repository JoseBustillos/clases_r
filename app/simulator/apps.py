from django.apps import AppConfig


class SimulatorConfig(AppConfig):
    name = 'app.simulator'
    verbose_name = "Simulador"
