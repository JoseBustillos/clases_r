from ckeditor.fields import RichTextField
from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from django.urls import reverse


class Classes(models.Model):
    user = models.ForeignKey(User, verbose_name="Usuario", on_delete=models.CASCADE)
    title = models.CharField(verbose_name="Titulo del curso", max_length=400)
    description = models.TextField(verbose_name="Descripcion del curso")
    imagen_course = models.ImageField(upload_to='course/')
    cap_pdf = models.FileField(verbose_name="Capitulo", upload_to="pdf", blank=True, null=True, )
    example_csv = models.FileField(verbose_name="Ejemplo en csv", upload_to="csv", blank=True, null=True)
    example_r = models.FileField(verbose_name="Ejemplo en R", upload_to="r", blank=True, null=True)
    content = RichTextField(blank=True, null=True)

    class Meta:
        verbose_name = "Clase"
        verbose_name_plural = "Clases"

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("classes-detail", kwargs={'pk': self.pk})
